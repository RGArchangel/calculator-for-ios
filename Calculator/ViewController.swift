//
//  ViewController.swift
//  Calculator
//
//  Created by Archangel on 03.02.2019.
//  Copyright © 2019 Archangel. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var display: UITextField!
    
    ///////////////////// NUMBER PROPERTIES /////////////////////////////////
    var notStartTyping : Bool = false
    var isValueNegative : Bool = false
    var isValueFractional : Bool = false
    var changeAction : Bool = false
    ////////////////////////////////////////////////////////////////////////
    
    
    ////////////////////// FOR OPERATIONS ON NUMS ///////////////////////////
    var firstVal : Double = 0
    var secondVal : Double = 0
    var result : Double = 0
    var act : NSString = "0"
    /////////////////////////////////////////////////////////////////////////
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //For 0:
    @IBAction func onButton0Touch(sender: AnyObject)
    {
        if !notStartTyping
        {
            display.text = "0"
            notStartTyping = true
        }
        else
        {
            if display.text != "0"
            {display.text = display.text! + "0"}
        }
    }
    
    //For 1:
    @IBAction func onButton1Touch(sender: AnyObject)
    {
        if !notStartTyping
        {
            display.text = "1"
            notStartTyping = true
        }
        else
        {
            if display.text == "0"
            {display.text = ""}
            display.text = display.text! + "1"
        }
    }
    
    //For 2:
    @IBAction func onButton2Touch(sender: AnyObject)
    {
        if !notStartTyping
        {
            display.text = "2"
            notStartTyping = true
        }
        else
        {
            if display.text == "0"
            {display.text = ""}
            display.text = display.text! + "2"
        }
    }
    
    //For 3:
    @IBAction func onButton3Touch(sender: AnyObject)
    {
        if !notStartTyping
        {
            display.text = "3"
            notStartTyping = true
        }
        else
        {
            if display.text == "0"
            {display.text = ""}
            display.text = display.text! + "3"
        }
    }
    
    //For 4:
    @IBAction func onButton4Touch(sender: AnyObject)
    {
        if !notStartTyping
        {
            display.text = "4"
            notStartTyping = true
        }
        else
        {
            if display.text == "0"
            {display.text = ""}
            display.text = display.text! + "4"
        }
    }
    
    //For 5:
    @IBAction func onButton5Touch(sender: AnyObject)
    {
        if !notStartTyping
        {
            display.text = "5"
            notStartTyping = true
        }
        else
        {
            if display.text == "0"
            {display.text = ""}
            display.text = display.text! + "5"
        }
    }
    
    //For 6:
    @IBAction func onButton6Touch(sender: AnyObject)
    {
        if !notStartTyping
        {
            display.text = "6"
            notStartTyping = true
        }
        else
        {
            if display.text == "0"
            {display.text = ""}
            display.text = display.text! + "6"
        }
    }
    
    //For 7:
    @IBAction func onButton7Touch(sender: AnyObject)
    {
        if !notStartTyping
        {
            display.text = "7"
            notStartTyping = true
        }
        else
        {
            if display.text == "0"
            {display.text = ""}
            display.text = display.text! + "7"
        }
    }
    
    //For 8:
    @IBAction func onButton8Touch(sender: AnyObject)
    {
        if !notStartTyping
        {
            display.text = "8"
            notStartTyping = true
        }
        else
        {
            if display.text == "0"
            {display.text = ""}
            display.text = display.text! + "8"
        }
    }
    
    //For 9:
    @IBAction func onButton9Touch(sender: AnyObject)
    {
        if !notStartTyping
        {
            display.text = "9"
            notStartTyping = true
        }
        else
        {
            if display.text == "0"
            {display.text = ""}
            display.text = display.text! + "9"
        }
    }
    
    //For .:
    @IBAction func onButtonDotTouch(sender: AnyObject)
    {
        if !notStartTyping
        {
            display.text = "0."
            notStartTyping = true
            isValueFractional = true
        }
        else
        {
            if !isValueFractional
            {
                display.text = display.text! + "."
                isValueFractional = true
            }
        }
    }
    
    //Clear
    @IBAction func onButtonClearTouch(sender: AnyObject)
    {
        notStartTyping = false
        isValueFractional = false
        isValueNegative = false
        display.text = "0"
        firstVal = 0
        secondVal = 0
        result = 0
        act = "0"
    }
    
    //Negative
    @IBAction func onButtonNegativeTouch(sender: AnyObject)
    {
        if act != "0"
        {
            display.text!.remove(at: display.text!.index(before: display.text!.endIndex))
            display.text!.remove(at: display.text!.index(before: display.text!.endIndex))
        }
        
        if !isValueNegative
        {
            let s : String = display.text!
            display.text = "-" + s
            isValueNegative = true
        }
        else
        {
            var x : Double = Double(display.text!)!
            x *= -1
            
            if x.truncatingRemainder(dividingBy: 1) != 0
            {
                isValueFractional = true
                let s : String = String(x)
                display.text = s
            }
            else
            {
                let a = Int(x)
                let s : String = String(a)
                display.text = s
            }
            
            isValueNegative = false
        }
        
        firstVal = 0
        secondVal = 0
        result = 0
        act = "0"
    }
    
    //Percent
    @IBAction func onButtonPercentTouch(sender: AnyObject)
    {
        firstVal = 0
        secondVal = 0
        result = 0
        act = "0"
        
        var x : Double = Double(display.text!)!
        x /= 100
        
        if x.truncatingRemainder(dividingBy: 1) != 0
        {
            isValueFractional = true
            let s : String = String(x)
            display.text = s
        }
        else
        {
            let a = Int(x)
            let s : String = String(a)
            display.text = s
        }
    }
    
    //For operation series
    func nextOperation()
    {
        if notStartTyping == true
        {
            secondVal = Double(display.text!)!
            
            if (act == "+")
            {
                result = firstVal + secondVal
            }
            
            if (act == "-")
            {
                result = firstVal - secondVal
            }
            
            if (act == "*")
            {
                result = firstVal * secondVal
            }
            
            if (act == "/")
            {
                result = firstVal / secondVal
            }
            
            if result.truncatingRemainder(dividingBy: 1) != 0
            {
                isValueFractional = true
                let s : String = String(result)
                display.text = s
            }
            else
            {
                let a = Int(result)
                let s : String = String(a)
                display.text = s
            }
        }
        else
        {changeAction = true}
        
        act = "0"
        notStartTyping = false
        secondVal = 0
        result = 0
    }
    
    //Plus
    @IBAction func onButtonPlusTouch(sender: AnyObject)
    {
        if (act != "0")
        {nextOperation()}
        
        if changeAction == true
        {
            display.text!.remove(at: display.text!.index(before: display.text!.endIndex))
            display.text!.remove(at: display.text!.index(before: display.text!.endIndex))
        }
        
        firstVal = Double(display.text!)!
        display.text = display.text! + " +"
        act = "+"
        
        notStartTyping = false
        isValueFractional = false
        changeAction = false
    }
    
    //Minus
    @IBAction func onButtonMinusTouch(sender: AnyObject)
    {
        if (act != "0")
        {nextOperation()}
        
        if changeAction == true
        {
            display.text!.remove(at: display.text!.index(before: display.text!.endIndex))
            display.text!.remove(at: display.text!.index(before: display.text!.endIndex))
        }
        
        firstVal = Double(display.text!)!
        display.text = display.text! + " -"
        act = "-"
        
        notStartTyping = false
        isValueFractional = false
        changeAction = false
    }
    
    //Multiplication
    @IBAction func onButtonMultipTouch(sender: AnyObject)
    {
        if (act != "0")
        {nextOperation()}
        
        if changeAction == true
        {
            display.text!.remove(at: display.text!.index(before: display.text!.endIndex))
            display.text!.remove(at: display.text!.index(before: display.text!.endIndex))
        }
        
        firstVal = Double(display.text!)!
        display.text = display.text! + " *"
        act = "*"
        
        notStartTyping = false
        isValueFractional = false
        changeAction = false
    }
    
    //Divide
    @IBAction func onButtonDivideTouch(sender: AnyObject)
    {
        if (act != "0")
        {nextOperation()}
        
        if changeAction == true
        {
            display.text!.remove(at: display.text!.index(before: display.text!.endIndex))
            display.text!.remove(at: display.text!.index(before: display.text!.endIndex))
        }
        
        firstVal = Double(display.text!)!
        display.text = display.text! + " /"
        act = "/"
        
        notStartTyping = false
        isValueFractional = false
        changeAction = false
    }
    
    //Equal
    @IBAction func onButtonEqualTouch(sender: AnyObject)
    {
        switch act {
        case "+":
            secondVal = Double(display.text!)!
            result = firstVal + secondVal
        case "-":
            secondVal = Double(display.text!)!
            result = firstVal - secondVal
        case "*":
            secondVal = Double(display.text!)!
            result = firstVal * secondVal
        case "/":
            secondVal = Double(display.text!)!
            result = firstVal / secondVal
        case "0":
            result = firstVal
        default:
            result = firstVal
        }
        
        if result.truncatingRemainder(dividingBy: 1) != 0
        {
            isValueFractional = true
            let s : String = String(result)
            display.text = s
        }
        else
        {
            let a = Int(result)
            let s : String = String(a)
            display.text = s
        }
        
        act = "0"
        firstVal = 0
        secondVal = 0
        result = 0
        changeAction = false
    }
}


